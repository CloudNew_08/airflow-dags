import json
from datetime import timedelta
from airflow import DAG
from airflow.models import Variable
from airflow.contrib.kubernetes import secret
from airflow.contrib.operators import kubernetes_pod_operator

from airflow.utils.dates import days_ago, parse_execution_date

datasets_cfg = Variable.get('datasets_conf', deserialize_json=True)

secret_volume = secret.Secret(
    'volume',
    # Path where we mount the secret as volume
    '/mnt/footprints/azure-secrets',
    # Name of Kubernetes Secret
    'azure-secret',
    # Key in the form of service account file name
    'azure-storage-keys.json')

def create_dag(dag_id,
               start_date,
               schedule_interval,
               default_args,
               storage_name,
               container_name,
               dataset_name,
               model_bucket,
               learning_image,
               train_script,
               train_extra_args,
               dataset_blob,
               model_blob):

  if train_script is None:
    return None

  dag = DAG(
      dag_id,
      # start_date=start_date,
      default_args=default_args,
      # description='E',
      schedule_interval=schedule_interval,
  )
  with dag:
    # If a Pod fails to launch, or has an error occur in the container, Airflow
    # will show the task as failed, as well as contain all of the task logs
    # required to debug.
    # Only name, namespace, image, and task_id are required to create a
    # KubernetesPodOperator. In Cloud Composer, currently the operator defaults
    # to using the config file found at `/home/airflow/composer_kube_config if
    # no `config_file` parameter is specified. By default it will contain the
    # credentials for Cloud Composer's Google Kubernetes Engine cluster that is
    # created upon environment creation.

    kubernetes_min_pod = kubernetes_pod_operator.KubernetesPodOperator(
        task_id='mf-train',
        name='airflow_mf-train',
        namespace='default',
        image=learning_image,
        cmds=['python'],
        arguments=[
            train_script,
            f'--train-data-dir={container_name}',
            f'--train-dataset-name={dataset_blob}',
            f'--model-dir={model_bucket}/{model_blob}',
            ] + train_extra_args,
        startup_timeout_seconds=300,
        # The secrets to pass to Pod, the Pod will fail to create if the
        # secrets you specify in a Secret object do not exist in Kubernetes.
        secrets=[secret_volume],
        # env_vars allows you to specify environment variables for your
        # container to use. env_vars is templated.
        env_vars={
            'AZURE_APPLICATION_CREDENTIALS': '/mnt/footprints/azure-secrets/azure-storage-keys.json'},
        in_cluster=False
    )
  return dag


# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
_default_args = {
    'owner': 'airflow',
    # 'depends_on_past': True,
    'start_date': days_ago(1),
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    # 'retries': 1,
    # 'retry_delay': timedelta(minutes=1),
}

for _business_client, business_conf in datasets_cfg.items():
  _storage_name = business_conf['storage_name']
  _container_name = business_conf['container_name']
  _blob_name = business_conf['blob_name']
  _dataset_name=business_conf['dataset_name']
  _model_name=business_conf['model_name']
  _model_bucket = business_conf.get('model_bucket')
  _cloud_storage_conn_id = business_conf['cloud_storage_conn_id']
  _ffr_conn_id = business_conf['ffr_conn_id']
  _ra_gateway_conn_id = business_conf['ra_gateway_conn_id']
  _learning_image = business_conf.get('learning_image')

  for location_conf in business_conf['locations']:
    _location_id = location_conf['id']
    _ra_building_conn_id = location_conf['ra_building_conn_id']
    _start_date = location_conf['start_date']
    _schedule_interval = location_conf['schedule_interval']

    for model_conf in location_conf['models']:
      _interaction_type = model_conf['interaction_type']
      _interaction_field = model_conf.get('interaction_field')
      _filename = model_conf['filename']
      _file_ext = model_conf['file_extension']
      _train_script = model_conf.get('train_script')
      _train_extra_args = model_conf.get('train_extra_args', [])
      _model_custom_prefix = model_conf.get('model_custom_prefix')

      _dataset_blob_prefix = f'{_business_client}/{_location_id}/{_interaction_type}/{_filename}'
      _model_blob = f'{_business_client}/{_location_id}/{_interaction_type}/{_model_custom_prefix}' \
                    if _model_custom_prefix else _dataset_blob_prefix
      _dag_id = f"train_{_model_blob.replace('/', '_')}"

      _dag = create_dag(
        dag_id=_dag_id,
        start_date=None,  # parse_execution_date(_start_date),
        schedule_interval=_schedule_interval,
        default_args=_default_args,
        storage_name=_storage_name,
        container_name=_container_name,
        dataset_name=_dataset_name,
        model_bucket=_model_bucket,
        train_script=_train_script,
        train_extra_args=_train_extra_args,
        learning_image=_learning_image,
        dataset_blob=f'{_dataset_blob_prefix}.{_file_ext}',
        model_blob=_model_blob
      )
      if _dag is not None:
        globals()[_dag_id] = _dag
