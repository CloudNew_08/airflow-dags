from airflow.plugins_manager import AirflowPlugin
from .operators.rav_to_wasb import RetailAnalyticsVisitsToWasbOperator

class AirflowFFRPlugin(AirflowPlugin):
  name = "ffr_plugin"  # does not need to match the package name
  operators = [RetailAnalyticsVisitsToWasbOperator]
  sensors = []
  hooks = []
  executors = []
  macros = []
  admin_views = []
  flask_blueprints = []
  menu_links = []
  appbuilder_views = []
  appbuilder_menu_items = []
  global_operator_extra_links = []
  operator_extra_links = []
